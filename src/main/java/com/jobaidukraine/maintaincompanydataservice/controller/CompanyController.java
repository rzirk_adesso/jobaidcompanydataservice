package com.jobaidukraine.maintaincompanydataservice.controller;

import com.jobaidukraine.maintaincompanydataservice.Dto.CompanyDto;
import com.jobaidukraine.maintaincompanydataservice.Dto.UserDto;
import com.jobaidukraine.maintaincompanydataservice.Dto.companyAdressDto;
import com.jobaidukraine.maintaincompanydataservice.mapper.CompanyAdressDtoToCompanyDtoMapper;
import com.jobaidukraine.maintaincompanydataservice.service.CompanyService;
import com.jobaidukraine.maintaincompanydataservice.service.GeoService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class CompanyController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private CompanyAdressDtoToCompanyDtoMapper mapper;


    @Autowired
    GeoService geoService;


    @GetMapping("/register-your-company")
    public String getCreateCompany(Model model) {
        model.addAttribute("companyAdressDto", new companyAdressDto());
        return "create_company";
    }

    @PostMapping("/company/create")
    public String postNewCompany(@ModelAttribute("companyAdressDto") companyAdressDto companyAdressDto, Model model){
        if (companyAdressDto.isTerms()){
        System.out.println(companyAdressDto.getEmail());
        System.out.println(companyAdressDto.getName());
        geoService.updateCoordinates(companyAdressDto);
        System.out.println("longitude: " + String.valueOf(companyAdressDto.getAddressDto().getLongitude()));
        System.out.println("latitude : " + String.valueOf(companyAdressDto.getAddressDto().getLatitude()));
        model.addAttribute("companyAdressDto", companyAdressDto);

        CompanyDto companyDto = mapper.companyAdressDtoToCompanyDto(companyAdressDto);
        UserDto userDto = new UserDto();
        userDto.setId(0l);
        companyDto.setContactUser(userDto);
        companyService.createCompany(companyDto);
        return "maintain_company";
        }
        else{
            return "/company/create";
        }
    }


    @GetMapping("/citys/{city}")
    public ResponseEntity<Set<String>> getCitys(@PathVariable(value = "city") String city) {
        return ResponseEntity.of(Optional.of(geoService.getTownNamesForString(city)));

    }



    @GetMapping("/maintain-your-company")
    public String getMaintainCompany(Model model) {
        CompanyDto companyDto = companyService.getCompany((Long) model.getAttribute("id"));
        companyAdressDto companyAdressDto = mapper.companyDtoToCompanyAdressDto(companyDto);
        model.addAttribute("companyAdressDto", companyAdressDto);
        return "maintain_company";
    }

}
