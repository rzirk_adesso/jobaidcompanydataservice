package com.jobaidukraine.maintaincompanydataservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "core")
public class CoreConfiguration {
    private String companyUrl;
}
