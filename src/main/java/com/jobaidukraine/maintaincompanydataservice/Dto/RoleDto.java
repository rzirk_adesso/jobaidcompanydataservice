package com.jobaidukraine.maintaincompanydataservice.Dto;

public enum RoleDto {
  ADMIN,
  EDITOR,
  USER,
}
