package com.jobaidukraine.maintaincompanydataservice.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;
  private Boolean deleted;
  private String email;
  private String firstname;
  private String lastname;
  private String position;
  private String country;
  private RoleDto role;
  private ExperienceDto experience;
  private Set<LanguageLevelDto> languageLevels;
}
