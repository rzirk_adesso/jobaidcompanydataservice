package com.jobaidukraine.maintaincompanydataservice.Dto;

public enum ExperienceDto {
  PROFESSIONAL,
  NEW_GRADUATE,
  STUDENT
}
