package com.jobaidukraine.maintaincompanydataservice.Dto;

public enum LanguageSkillDto {
  NATIVE,
  FLUENT,
  GOOD,
  BASIC
}
