package com.jobaidukraine.maintaincompanydataservice.Dto;

import lombok.Data;

@Data
public class AddressDto {

    private float longitude;
    private float latitude;
    private String street;
    private int houseNumber;
    private int zip;
    private String city;
    private  String country;

}
