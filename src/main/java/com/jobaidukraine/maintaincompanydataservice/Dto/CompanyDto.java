package com.jobaidukraine.maintaincompanydataservice.Dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CompanyDto {

  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;
  private Boolean deleted;
  private String name;
  private String url;
  private String email;
  private String logo;
  public boolean terms;
  private Set<JobDto> jobs;
  private AddressDto address;
  private UserDto contactUser;
}
