package com.jobaidukraine.maintaincompanydataservice.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LanguageLevelDto {
  private String language;
  private LanguageSkillDto languageSkill;
}
