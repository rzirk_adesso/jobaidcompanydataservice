package com.jobaidukraine.maintaincompanydataservice.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobDto {
  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;
  private Boolean deleted;
  private String title;
  private String description;
  private String jobType;
  private String applicationLink;
  private String applicationMail;
  private String videoUrl;
  private AddressDto address;
  private Set<String> qualifications;
  private String classification;
  private Set<LanguageLevelDto> languageLevels;
}
