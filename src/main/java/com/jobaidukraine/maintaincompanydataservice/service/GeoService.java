package com.jobaidukraine.maintaincompanydataservice.service;

import com.jobaidukraine.maintaincompanydataservice.Dto.Town;
import com.jobaidukraine.maintaincompanydataservice.Dto.companyAdressDto;
import com.jobaidukraine.maintaincompanydataservice.constants.RestEndpoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class GeoService {

    @Autowired
    private RestTemplate restTemplate;



    public List<Town> getTownsForString(String townString){
        ResponseEntity<Town[]> responseEntity = restTemplate.getForEntity(RestEndpoints.GEO_SERVICE_ADRESS +"/town/"+townString, Town[].class);
        Town[] towns = responseEntity.getBody();

        return List.of(towns);
    }

    public Set<String> getTownNamesForString(String city){
        List<Town> towns = getTownsForString(city);
        Set<String> townNames = new HashSet<>();
        for(Town town: towns){
            townNames.add(town.town);
        }
        return townNames;
    }


    public void updateCoordinates(companyAdressDto companyAdressDto){
        System.out.println(restTemplate.getForEntity(RestEndpoints.GEO_SERVICE_ADRESS+"/zip/"+String.valueOf(companyAdressDto.getAddressDto().getZip()), String.class));
        ResponseEntity<Town[]> responseEntity = restTemplate.getForEntity(RestEndpoints.GEO_SERVICE_ADRESS+"/zip/"+String.valueOf(companyAdressDto.getAddressDto().getZip()), Town[].class);
        Town[] towns = responseEntity.getBody();
        if(towns.length==0)return;
        Town town = towns[0];
        if(town==null)return;
        companyAdressDto.getAddressDto().setLongitude(town.lng);
        companyAdressDto.getAddressDto().setLatitude(town.lat);
    }

    public Town getTownForZipCode(String zipCode){
        ResponseEntity<Town> responseEntity = restTemplate.getForEntity(RestEndpoints.GEO_SERVICE_ADRESS+"/zip/"+zipCode, Town.class);
        return responseEntity.getBody();
    }


}

