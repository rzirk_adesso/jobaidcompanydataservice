package com.jobaidukraine.maintaincompanydataservice.service;

import com.jobaidukraine.maintaincompanydataservice.Dto.CompanyDto;
import com.jobaidukraine.maintaincompanydataservice.Dto.companyAdressDto;
import com.jobaidukraine.maintaincompanydataservice.core.CoreClient;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.hateoas.EntityModel;

@Service
@AllArgsConstructor
public class CompanyService {

    private final CoreClient coreClient;

    public void createCompany(CompanyDto companyDto) {
        coreClient.createNewCompany(companyDto);
    }

    public CompanyDto getCompany(Long id) {
        EntityModel<CompanyDto> companyEntity = coreClient.getCompany(id);
        return companyEntity.getContent();
    }
}
