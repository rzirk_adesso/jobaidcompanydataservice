package com.jobaidukraine.maintaincompanydataservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaintainCompanyDataServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaintainCompanyDataServiceApplication.class, args);
	}

}
