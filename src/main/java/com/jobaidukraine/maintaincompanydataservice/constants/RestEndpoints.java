package com.jobaidukraine.maintaincompanydataservice.constants;

public class RestEndpoints {

    public static final String API_VERSION = "1.0";
    public static final String CORE_API_ADDRESS = "http://localhost:8080";
    public static final String GEO_SERVICE_ADRESS = "http://localhost:3001/";
    public static final String COMPANY_ENDPOINT = "/companies";
}
