package com.jobaidukraine.maintaincompanydataservice.mapper;

import com.jobaidukraine.maintaincompanydataservice.Dto.CompanyDto;
import com.jobaidukraine.maintaincompanydataservice.Dto.companyAdressDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;

@Mapper(componentModel = "spring",
        nullValueMappingStrategy = RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.ERROR
)
public interface CompanyAdressDtoToCompanyDtoMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)

    @Mapping(target = "version", ignore = true)
    @Mapping( target = "deleted", ignore = true)
    @Mapping( target = "jobs", ignore = true)
    @Mapping(source = "addressDto", target = "address")
    @Mapping(target = "contactUser", ignore = true)
    @Mapping(target = "terms",source = "terms",ignore = true)
    CompanyDto companyAdressDtoToCompanyDto(companyAdressDto companyAdressDto);
    @Mapping(source = "address", target = "addressDto")
    companyAdressDto companyDtoToCompanyAdressDto(CompanyDto companyDto);
}