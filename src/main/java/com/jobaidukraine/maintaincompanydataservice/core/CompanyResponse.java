package com.jobaidukraine.maintaincompanydataservice.core;

import com.jobaidukraine.maintaincompanydataservice.Dto.CompanyDto;
import lombok.Getter;
import org.springframework.hateoas.EntityModel;

@Getter
public class CompanyResponse {
    EntityModel<CompanyDto> company;
}
