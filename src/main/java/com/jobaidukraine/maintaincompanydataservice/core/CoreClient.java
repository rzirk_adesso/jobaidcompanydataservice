package com.jobaidukraine.maintaincompanydataservice.core;

import com.jobaidukraine.maintaincompanydataservice.Dto.CompanyDto;
import com.jobaidukraine.maintaincompanydataservice.config.CoreConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.hateoas.EntityModel;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CoreClient {
    private final CoreConfiguration coreConfiguration;
    private final RestTemplate restTemplate;

    public EntityModel<CompanyDto> createNewCompany(CompanyDto company) {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<CompanyDto> entity = new HttpEntity<>(company, headers);
        ResponseEntity<CompanyResponse> response = restTemplate.postForEntity(
                coreConfiguration.getCompanyUrl(), entity, CompanyResponse.class);

        return response.getBody().getCompany();
    }

    public EntityModel<CompanyDto> getCompany(Long id) {
        HttpHeaders headers = new HttpHeaders();
        final HttpEntity<Void> entity = new HttpEntity<>(headers);
        Map<String, Long> params = new HashMap<String, Long>();
        params.put("id", id);
        ResponseEntity<CompanyResponse> response = this.restTemplate.exchange(coreConfiguration.getCompanyUrl(), HttpMethod.GET, entity, CompanyResponse.class, params);

        return response.getBody().getCompany();
    }
}
